# DevopsWorks GitLab template repository

[![build status](https://gitlab.com/devopsworks/templates/gitlab-ci/badges/master/pipeline.svg)](https://gitlab.com/devopsworks/templates/gitlab-ci/commits/master)

This repository contains `gitlab-ci` templates.

## Templates

### Ansible deployment

| file                         | job             | description                             |
| ---------------------------- | --------------- | --------------------------------------- |
| ansible/.gitlab-ci_deploy.md | .deploy_cache   | See [doc](ansible/.gitlab-ci_deploy.md) |
| ansible/.gitlab-ci_deploy.md | .before_script  | See [doc](ansible/.gitlab-ci_deploy.md) |
| ansible/.gitlab-ci_deploy.md | .deploy_script  | See [doc](ansible/.gitlab-ci_deploy.md) |
| ansible/.gitlab-ci_deploy.md | .deploy_staging | See [doc](ansible/.gitlab-ci_deploy.md) |
| ansible/.gitlab-ci_deploy.md | .deploy_prod    | See [doc](ansible/.gitlab-ci_deploy.md) |

### Code

| file            | job                   | description                                                                                             |
| --------------- | --------------------- | ------------------------------------------------------------------------------------------------------- |
| code/generic.md | .code_depscan         | [GitLab dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) |
| code/generic.md | .code_quality         | [GitLab Code Quality testing](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) |
| code/generic.md | .code_sast            | [GitLab SAST](https://docs.gitlab.com/ee/user/application_security/sast/)                               |
| code/golang.yml | .golang_cilint        | Linting using [GolangCI-Lint](https://github.com/golangci/golangci-lint                                 |
| code/golang.yml | .golang_vet           | Go Vet                                                                                                  |
| code/golang.yml | .golang_test          | Runs tests with parallelism                                                                             |
| code/golang.yml | .golang_test_mono     | Runs tests **without** parallelism                                                                      |
| code/golang.yml | .golang_coverage      | Runs coverage tests and generate report                                                                 |
| code/golang.yml | .golang_coverage_mono | Runs coverage tests **without** and generate report                                                     |
| code/golang.yml | .golang_docker_build  | Builds docker image injecting `main.Version` & `main.BuildDate`                                         |

### Testing

| file               | job              | description                                                   |
| ------------------ | ---------------- | ------------------------------------------------------------- |
| testing/linters.md | .lint_yaml       | [YAML Linter](https://yamllint.readthedocs.io/en/stable/)     |
| testing/linters.md | .lint_markdown   | [Markdown Lint](https://github.com/markdownlint/markdownlint) |
| testing/linters.md | .lint_shellcheck | [ShellCheck](https://www.shellcheck.net/)                     |
| testing/linters.md | .lint_ansible    | [Ansible Lint](https://github.com/ansible/ansible-lint)       |
